package com.android.sachin.geotaggingapp;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by SACHIN MUTTHE on 17-02-2018.
 */

public class ImageListHolder extends RecyclerView.ViewHolder {

    public ImageView image_view_tagged_image;
    public TextView text_view_lat_long;
    public CardView card_view_main;

    public ImageListHolder(View itemView) {
        super(itemView);

        image_view_tagged_image = itemView.findViewById(R.id.image_view_tagged_image);
        text_view_lat_long = itemView.findViewById(R.id.text_view_lat_long);
        card_view_main = itemView.findViewById(R.id.card_view_main);
    }
}

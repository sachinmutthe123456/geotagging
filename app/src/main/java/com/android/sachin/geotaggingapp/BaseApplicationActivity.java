package com.android.sachin.geotaggingapp;

import android.app.Application;
import android.util.Log;

/**
 * Created by SACHIN MUTTHE on 17-02-2018.
 */

public class BaseApplicationActivity extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("DEBUG", "App Started");
        DataService.setSharedPreferenceManager(BaseApplicationActivity.this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.d("DEBUG", "App Terminated");
    }
}

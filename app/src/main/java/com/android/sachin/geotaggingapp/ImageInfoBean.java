package com.android.sachin.geotaggingapp;

import java.io.Serializable;

/**
 * Created by SACHIN MUTTHE on 17-02-2018.
 */

public class ImageInfoBean implements Serializable{

    String imagePath;
    double latitude,longitude;

    public ImageInfoBean(String imagePath, double latitude, double longitude) {
        this.imagePath = imagePath;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}

package com.android.sachin.geotaggingapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.maps.android.ui.IconGenerator;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class MapActivity extends AppCompatActivity implements GoogleMap.OnMarkerClickListener, OnMapReadyCallback {

    private GoogleMap mMap;
    boolean firstTimeZoom = false;
    private static final int PERMISSION_REQUEST_CODE = 10;
    private static String[] reqPermissions = {android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
    LatLng clickedLatLong;
    ImageInfoBean imageInfoBean = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_activity);

        imageInfoBean = (ImageInfoBean) getIntent().getSerializableExtra("selected_image");

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                ) {
            // Some permission has not been granted.
            //Toast.makeText(HomeActivity.this, "Permissions NOT Granted", Toast.LENGTH_SHORT).show();
            requestSomePermission();
        } else {
            // permission was granted, yay! Do the
            // contacts-related task you need to do.
            //Toast.makeText(HomeActivity.this, "All Permissions Granted", Toast.LENGTH_SHORT).show();
            initMap();
        }
    }

    private void initMap() {
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Intent listActivity = new Intent(MapActivity.this, ImageListActivity.class);
        startActivity(listActivity);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (imageInfoBean == null) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                googleMap.setMyLocationEnabled(true);

                GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
                    @Override
                    public void onMyLocationChange(Location location) {
                        if (!firstTimeZoom) {
                            LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
                            firstTimeZoom = true;
                        }
                    }
                };
                mMap.setOnMyLocationChangeListener(myLocationChangeListener);
            }
        } else {
            LatLng loc = new LatLng(imageInfoBean.getLatitude(), imageInfoBean.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
        }

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                //Toast.makeText(MapActivity.this, "" + latLng.latitude + ", " + latLng.longitude, Toast.LENGTH_SHORT).show();
                clickedLatLong = latLng;
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivityForResult(intent, 0);
            }
        });

        addMarkers();
    }

    public void addMarkers() {

        try {
            if (DataService.getSharedPreferenceManager().getImageInfo() != null) {
                JSONArray imageInfoJsonArray = new JSONArray(DataService.getSharedPreferenceManager().getImageInfo());

                for (int i = 0; i < imageInfoJsonArray.length(); i++) {
                    ImageInfoBean infoBean = new Gson().fromJson(imageInfoJsonArray.get(i).toString(), ImageInfoBean.class);
                    Bitmap myBitmap = BitmapFactory.decodeFile(String.valueOf(new File(infoBean.getImagePath())));
                    IconGenerator iconFactory = new IconGenerator(MapActivity.this);
                    LatLng pos = new LatLng(infoBean.getLatitude(), infoBean.getLongitude());
                    mMap.addMarker(new MarkerOptions()
                            .position(pos)
                            .title("")
                            .icon(BitmapDescriptorFactory.fromBitmap(myBitmap)));
                            /*.snippet(snippet))*//*.showInfoWindow()*//*;*/
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && requestCode == 0) {
            /*String result = data.toURI();
            Toast.makeText(this, "" + result, Toast.LENGTH_SHORT).show();*/

            Bitmap photo = (Bitmap) data.getExtras().get("data");

            // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
            Uri tempUri = getImageUri(getApplicationContext(), photo);

            // CALL THIS METHOD TO GET THE ACTUAL PATH
            File finalFile = new File(getRealPathFromURI(tempUri));

            //Toast.makeText(this, "" + finalFile.getAbsolutePath(), Toast.LENGTH_SHORT).show();
            ImageInfoBean imageInfoBean = new ImageInfoBean(finalFile.getAbsolutePath(), clickedLatLong.latitude, clickedLatLong.longitude);

            JSONArray imageInfoJsonArrayNew = new JSONArray();
            try {
                if (DataService.getSharedPreferenceManager().getImageInfo() != null) {
                    JSONArray imageInfoJsonArrayOld = new JSONArray(DataService.getSharedPreferenceManager().getImageInfo());
                    for (int i = 0; i < imageInfoJsonArrayOld.length(); i++) {
                        imageInfoJsonArrayNew.put(imageInfoJsonArrayOld.get(i)); // Adding to new JSONArray
                    }

                    imageInfoJsonArrayNew.put(new Gson().toJson(imageInfoBean));
                } else
                    imageInfoJsonArrayNew.put(new Gson().toJson(imageInfoBean));


                DataService.getSharedPreferenceManager().setImageInfo(imageInfoJsonArrayNew.toString());
                Toast.makeText(MapActivity.this, "Geo-Tagging Completed", Toast.LENGTH_SHORT).show();
                initMap();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private void requestSomePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                ) {
            Log.i("", "Displaying permission rationale to provide additional context.");

            ActivityCompat.requestPermissions(this, reqPermissions,
                    PERMISSION_REQUEST_CODE);

        } else
            ActivityCompat.requestPermissions(this, reqPermissions, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    //Toast.makeText(HomeActivity.this, "All Permissions Granted", Toast.LENGTH_SHORT).show();
                    initMap();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //Toast.makeText(HomeActivity.this, "All Permissions NOT Granted", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

}

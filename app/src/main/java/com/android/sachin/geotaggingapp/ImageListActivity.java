package com.android.sachin.geotaggingapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.maps.android.ui.IconGenerator;

import org.json.JSONArray;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SACHIN MUTTHE on 17-02-2018.
 */

public class ImageListActivity extends AppCompatActivity {

    RecyclerView recycler_view_image_list;
    Toolbar toolbar;
    TextView text_view_toolbar_title;
    ImageListAdapter imageListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_list_activity);

        recycler_view_image_list = (RecyclerView) findViewById(R.id.recycler_view_image_list);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        text_view_toolbar_title = (TextView) findViewById(R.id.text_view_toolbar_title);

        text_view_toolbar_title.setText("Geo-Tagged Image List");
        initToolBar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setData();
    }

    private void setData() {
        try {
            if (DataService.getSharedPreferenceManager().getImageInfo() != null) {
                List<ImageInfoBean> imageInfoBeanList = new ArrayList<>();
                JSONArray imageInfoJsonArray = new JSONArray(DataService.getSharedPreferenceManager().getImageInfo());

                for (int i = 0; i < imageInfoJsonArray.length(); i++) {
                    ImageInfoBean infoBean = new Gson().fromJson(imageInfoJsonArray.get(i).toString(), ImageInfoBean.class);
                    imageInfoBeanList.add(infoBean);
                }

                imageListAdapter = new ImageListAdapter(ImageListActivity.this, imageInfoBeanList);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                recycler_view_image_list.setLayoutManager(mLayoutManager);
                recycler_view_image_list.setAdapter(imageListAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void initToolBar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_white_48px);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish(); // close this activity and return to preview activity (if there is any)
        }
        return true;
    }
}

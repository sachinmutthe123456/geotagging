package com.android.sachin.geotaggingapp;

import android.content.Context;
import android.util.Log;

/**
 * Created by SACHIN MUTTHE on 17-02-2018.
 */

public class DataService {

    public static SharedPreferenceManager sharedPreferenceManager = null;

    public static SharedPreferenceManager getSharedPreferenceManager() {
        return sharedPreferenceManager;
    }

    public static void setSharedPreferenceManager(Context context) {
        if (sharedPreferenceManager == null)
            sharedPreferenceManager = new SharedPreferenceManager(context);
        Log.d("DEBUG", "Shared prefs set");
    }
}

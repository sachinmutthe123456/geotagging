package com.android.sachin.geotaggingapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.Collections;
import java.util.List;

/**
 * Created by SACHIN MUTTHE on 17-02-2018.
 */

public class ImageListAdapter extends RecyclerView.Adapter<ImageListHolder> {

    private LayoutInflater infilator;
    List<ImageInfoBean> data = Collections.emptyList();
    Context mContext;

    public ImageListAdapter(Context mContext, List<ImageInfoBean> data) {
        infilator = LayoutInflater.from(mContext);
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public ImageListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = infilator.inflate(R.layout.image_list_single_row_item, parent, false);
        ImageListHolder imageListHolder = new ImageListHolder(view);
        return imageListHolder;
    }

    @Override
    public void onBindViewHolder(final ImageListHolder holder, final int position) {
        final ImageInfoBean current = data.get(position);
        holder.text_view_lat_long.setText("Latitude : " + current.getLatitude() + "\nLongitude : " + current.getLongitude());

        Glide.with(mContext)
                .load(current.getImagePath())
                .into(holder.image_view_tagged_image);

        holder.card_view_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, MapActivity.class);
                intent.putExtra("selected_image", current);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}

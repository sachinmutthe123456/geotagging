package com.android.sachin.geotaggingapp;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by SACHIN MUTTHE on 17-02-2018.
 */

public class SharedPreferenceManager {

    private final SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context mContext;

    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "geotagapp";

    private static final String KEY_IMAGE_INFO = "image_info";

    public SharedPreferenceManager(Context mContext) {
        this.mContext = mContext;
        sharedPreferences = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void clearPreferences() {
        editor.clear();
        editor.commit();
    }

    public String getImageInfo() {
        return sharedPreferences.getString(KEY_IMAGE_INFO, null);
    }

    public void setImageInfo(String imageInfo) {
        editor.putString(KEY_IMAGE_INFO, imageInfo);
        editor.commit();
    }

}
